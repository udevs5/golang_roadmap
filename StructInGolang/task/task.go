package task

type Task struct {
	//ID, Name, Status, Priority, CreatedAt, CreatedBy, DueDate.
	ID                                                    int
	Name, Status, Priority, CreatedAt, CreatedBy, DueDate string
}

var tasks map[int]Task = make(map[int]Task)

// create, update, get, getAll and delete
func Create(task Task) {
	tasks[task.ID] = task
}
func Update(task Task) {
	tasks[task.ID] = task
}
func Get(ID int) Task {
	return tasks[ID]
}
func GetAll() []Task {
	var result []Task
	for _, element := range tasks {
		result = append(result, element)
	}
	return result
}
func Delete(ID int) {
	delete(tasks, ID)
}
