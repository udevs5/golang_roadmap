package tasksforoop

type TeamLead struct {
	Programmer
}

func (teamlead *TeamLead) DelegateTask(step string) (status string, err error) {
	return "", nil
}
