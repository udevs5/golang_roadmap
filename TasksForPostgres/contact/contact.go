package contact

type Contact struct {
	//ID, FirstName, LastName, Phone, Email, Position.
	ID                                          int
	FirstName, LastName, Phone, Email, Position string
}

var contacts map[int]Contact = make(map[int]Contact)

// create, update, get, getAll and delete
func Create(contact Contact) {
	contacts[contact.ID] = contact
}
func Update(contact Contact) {
	contacts[contact.ID] = contact
}
func Get(ID int) Contact {
	return contacts[ID]
}
func GetAll() []Contact {
	var result []Contact
	for _, element := range contacts {
		result = append(result, element)
	}
	return result
}
func Delete(ID int) {
	delete(contacts, ID)
}
