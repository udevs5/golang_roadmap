package Fibonacci

import (
	"fmt"
	"strconv"
)

func fibonacci(i int) int {
	first, second := 0, 1
	for i > 0 {
		first, second = second, first+second
		i--
	}
	return second

}
func main() {
	fmt.Println("fib:" + strconv.Itoa(fibonacci(5)))
}
