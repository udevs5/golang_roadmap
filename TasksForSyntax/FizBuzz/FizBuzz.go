package FizBuzz

import (
	"fmt"
)

func fizzbuzz(i int) {
	switch {
	case i%15 == 0:
		fmt.Println("fizzbuzz")
	case i%3 == 0:
		fmt.Println("fizz")
	case i%5 == 0:
		fmt.Println("buzz")
	default:
		fmt.Println(i)
	}

}
func main() {
	i := 40
	for i > 0 {
		fizzbuzz(i)
		i--
	}
}
