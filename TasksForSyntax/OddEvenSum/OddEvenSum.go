package OddEvenSum

import "fmt"

func Odd(i int) bool {
	return i%2 == 1
}
func Even(i int) bool {
	return i%2 == 0
}
func Sum(arr []int) int {
	res := 0
	for i := 0; i < len(arr); i++ {
		res += arr[i]
	}
	return res
}

func main() {
	fmt.Println(Odd(1))
	fmt.Println(Even(1))
	fmt.Println(Sum([]int{1, 2}))
}
